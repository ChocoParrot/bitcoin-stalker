import tensorflow as tf

import model
import reader
import auxils
import colours

import numpy as np

#auxils.getCsv()

def saveModel (sess):
    saver = tf.train.Saver()
    saver.save(sess, config["save-to"] + "/" + config["run-name"] + ".ckpt")

config = reader.getConfig()

iterator = auxils.Iterator().loadCsv()

print(colours.RED + "\nLoading model.\n" + colours.END)

m = model.Model()

print(colours.RED + "\nModel loaded.\n" + colours.END)

with tf.Session() as session:

    session.run(tf.global_variables_initializer())

    if config["visualise"]:
        print(colours.BLUE + colours.BOLD + "\nVisualisation enabled. Creating validation.\n" + colours.END)

        tf.summary.scalar("Predicted", tf.reduce_mean(m.expected))

        merged = tf.summary.merge_all()
        writer = tf.summary.FileWriter(config["tensorboard-directory"] + "/" + "Validation-S2", session.graph)

    for i in range(config["attenuation"]):
        input, output = iterator.getData()

        delta = session.run(m.delta, feed_dict={m.input: np.expand_dims(input, 0), m.expected: np.expand_dims(output, 0)})

        print(colours.RED + colours.BOLD + "[Attenuating] " + colours.END + "Iteration {}. Delta: {}".format(i, delta))

    for i in range(iterator.getSetCount()):
        input, output = iterator.getData()

        if config["visualise"]:
            summary = session.run(merged, feed_dict={m.input: np.expand_dims(input, 0), m.expected: np.expand_dims(output, 0)})
            writer.add_summary(summary, i)

        print(colours.GREEN + colours.BOLD + "[Creating Validation] " + colours.END + "Iteration {}. Delta: {}".format(i, delta))
