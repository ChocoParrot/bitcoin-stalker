import tensorflow as tf

import model
import reader
import auxils
import colours

import numpy as np

#auxils.getCsv()

def saveModel (sess):
    saver = tf.train.Saver()
    saver.save(sess, config["save-to"] + "/" + config["run-name"] + ".ckpt")

config = reader.getConfig()

iterator = auxils.Iterator().loadCsv(backload=4)

print(colours.RED + "\nLoading model.\n" + colours.END)

m = model.Model()

print(colours.RED + "\nModel loaded.\n" + colours.END)

configx = tf.ConfigProto()
configx.gpu_options.allow_growth = True

with tf.Session(config=configx) as session:

    session.run(tf.global_variables_initializer())

    state = session.run(m.initial_state)

    if config["visualise"]:
        print(colours.BLUE + colours.BOLD + "\nVisualisation enabled.\n" + colours.END)

        tf.summary.scalar("Delta", m.delta)
        tf.summary.scalar("Predicted", tf.reduce_mean(m.output))

        merged = tf.summary.merge_all()
        writer = tf.summary.FileWriter(config["tensorboard-directory"] + "/" + config["run-name"], session.graph)

    for i in range(config["attenuation"]):
        input, output = iterator.getData()

        delta, state = session.run([m.delta, m.state], feed_dict={m.input: np.expand_dims(input, 0), m.expected: np.expand_dims(output, 0), m.initial_state: state})

        print(colours.RED + colours.BOLD + "[Attenuating] " + colours.END + "Iteration {}. Delta: {}".format(i, delta))

    for i in range(iterator.getSetCount()):
        input, output = iterator.getData()

        if i % 5000 == 0 and i != 0:
            saveModel(session)

        if config["visualise"]:
            summary, delta, _, state = session.run([merged, m.delta, m.optimiser, m.state], feed_dict={m.input: np.expand_dims(input, 0), m.expected: np.expand_dims(output, 0), m.initial_state: state})
            writer.add_summary(summary, i)

        else:
            delta, _, state = session.run([m.delta, m.optimiser, m.state], feed_dict={m.input: np.expand_dims(input, 0), m.expected: np.expand_dims(output, 0), m.initial_state: state})

        print(colours.GREEN + colours.BOLD + "[Training] " + colours.END + "Iteration {}. Delta: {}".format(i, delta))
