import csv
import os
import math

import colours

data_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + "/data"
files = [data_dir + "/" + vx for vx in os.listdir(data_dir) if vx.endswith(".csv")]

class Iterator ():

    def __init__ (self):
        self.index = 0

    def loadCsv (self, backload=2):

        self.data = list()

        for i in range(len(files) - backload, len(files)):

            with open(files[i], "r") as file:
                print("\nAttempting to load: " + colours.BLUE + files[i] + colours.END)
                next(file)
                spamreader = csv.reader(file, delimiter=",")

                # timestamp, amount, price, symbol

                for row in spamreader:
                    self.data.append(row)

        print(("\n########################################\n\nLoaded " + colours.BOLD + "{}" + colours.END + " data points.\n\n########################################\n").format(len(self.data)))

        return self

    def getSetCount (self):
        return len(self.data)

    def getVerbose (self, relative=1, increment=False):
        data = self.data[self.index + relative]

        if increment:
            self.index += 1

        return data

    def getData (self):

        self.index += 1

        def getRel (rel=0):
            return float(self.data[self.index + rel][2])

        #print(self.data[self.index][0])
        return [(getRel(0) - getRel(-1))/getRel(-1) * 100], [(getRel(1) - getRel(0))/getRel(0) * 100000]
        #return [float(self.data[self.index][1])], [float(self.data[self.index + 1][1])]
