import tensorflow as tf

class Model ():

    def __init__ (self):

        self.batch_size = 1

        self.input = tf.placeholder(shape=[None, 1], dtype=tf.float32)
        self.expected = tf.placeholder(shape=[None, 1], dtype=tf.float32)

        recurrence, self.state, self.initial_state = self.create_recurrent_layers(self.input, 5, 40)

        self.output = tf.layers.dense(recurrence, 1, activation=None)
        self.loss = tf.losses.mean_squared_error(self.expected, self.output)

        self.delta = tf.abs(tf.reduce_mean(self.output - self.expected))

        self.optimiser = tf.train.AdamOptimizer(learning_rate=0.001).minimize(self.loss)

    def create_recurrent_layers (self, x, layers, units):

        assert isinstance(layers, int)
        assert isinstance(units, int)

        def cell():
          return tf.contrib.rnn.BasicLSTMCell(units)

        stacked_cells = tf.contrib.rnn.MultiRNNCell([cell() for _ in range(layers)])
        #stacked_cells = tf.contrib.rnn.GRUCell(units, activation=self.prelu)

        initial_state = stacked_cells.zero_state(self.batch_size, tf.float32)

        x = tf.reshape(x, [-1, self.batch_size, x.shape[1]])

        output, new_state = tf.nn.dynamic_rnn(stacked_cells, x, initial_state=initial_state, dtype=tf.float32, time_major=True)

        return tf.layers.flatten(output), new_state, initial_state

    """
    def create_recurrent_layers_b (self, x, layers, units, time_steps):

        assert isinstance(time_steps, int)

        self.state_placeholder = tf.placeholder(tf.float32, [layers, 2, None, time_steps])

        state_per_layer_list = tf.unstack(self.state_placeholder, axis=0)

        rnn_tuple_state = tuple(
            [tf.nn.rnn_cell.LSTMStateTuple(state_per_layer_list[idx][0], state_per_layer_list[idx][1])
             for idx in range(layers)]
        )

        init_state = np.zeros((layers, 2, batch_size, time_steps))

        cell = tf.contrib.rnn.LSTMCell(state_size, state_is_tuple=True)
        cell = tf.contrib.rnn.MultiRNNCell([cell] * layers, state_is_tuple=True)

        output, new_states = tf.nn.dynamic_rnn(stacked_lstm, x, initial_state=rnn_tuple_state)

        new_states = tf.stack(new_states)

        return output, new_states"""

    def bent_identity (self, x):
        return (tf.sqrt(tf.square(x) + 1) - 1) / 2 + x

    def prelu (self, _x, scope=None):
        """parametric ReLU activation"""
        with tf.variable_scope(name_or_scope=scope, default_name="prelu"):
            _alpha = tf.get_variable("prelu", shape=_x.get_shape()[-1],
                                     dtype=_x.dtype, initializer=tf.constant_initializer(0.1))
            return tf.maximum(0.0, _x) + _alpha * tf.minimum(0.0, _x)
