import json
import os

def read (dir):

    f = open(dir, "r")

    return json.load(f)

def getConfig ():

    path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

    print(path)

    return read(path + "/config.json")
